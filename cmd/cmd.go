package cmd

import (
	"gitlab.com/rumahlogic/go-cheat-sheet/utils"
	"io/ioutil"
	"os/exec"
	"strings"
	"time"
)

func GenerateChangeLogFromGitCommit(name, repo string) (err error) {
	// git log --pretty=format:"%h|%ad|%s"
	var (
		out 	[]byte
		data 	[]string
		temp 	string
		text 	= "\n# "+name+".md\n\n"
	)
	if out, err = exec.Command("bash", "-c", "git log --pretty=format:\"%H|%h|%ad|%s||\"").Output(); err != nil {
		utils.ErrorHandler(err)
		return
	}
	data = strings.Split(string(out), "||")
	for _, elm := range data {
		elmArr := strings.Split(elm, "|")
		if len(elmArr) < 4 { continue }
		date, _ := time.Parse("Mon Jan 02 15:04:05 2006 -0700 ", elmArr[2])
		if date.Format("2006-01-02") != temp {
			text += "### (" + date.Format("2006-01-02") + ")\n"
		}
		text += "  - "+elmArr[3] + " -> [" + elmArr[1] + "](" + repo + "/-/commit/" + strings.ReplaceAll(elmArr[0], "\n", "") + ")\n"
		temp = date.Format("2006-01-02")
	}


	return ioutil.WriteFile(name+".md", []byte(text), 0755)

}