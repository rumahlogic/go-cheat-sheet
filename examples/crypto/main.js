
// Cara pake :
// encrypt
//node nama.js e=text yg mau diencrypt

//decrypt
//node nama.js d=chipher text yg mau di decrypt


// console.log('19741023 200012 1 001')
// console.log(decrypt('1a572b1dcee4ffb18440683bfe99a9dbc0a10e4efc4633e4d5984ae52065d8ba'))


const crypto = require('crypto');

const key = crypto
    .createHash("sha256")
    .update('ahbd7asdhGDnsDJsdbSOpwr3293nsfjnNDSbhgsf8723bhsdbfvg')
    .digest();

const resizedIV = Buffer.allocUnsafe(16);
const iv = crypto
    .createHash("sha256")
    .update("myHashedIV")
    .digest();
iv.copy(resizedIV);

const encrypt = (value = '') => {
    const cypher = crypto.createCipheriv('aes256', key, resizedIV);
    return [cypher.update(value, 'utf-8', 'hex'),cypher.final('hex')].join('');
}

const decrypt = (value = '') => {
    const decipher = crypto.createDecipheriv('aes256', key, resizedIV);
    return [decipher.update(value, 'hex', 'utf-8'), decipher.final("utf-8")].join('');
}

const args = process.argv.slice(2)
const arr = args[0].split('=')

if (arr[0] === "e") {
    console.log(encrypt(arr[1]))
} else if ((arr[0] === "d")) {
    console.log(decrypt(arr[1]))
} else {
    console.error("err")
}

