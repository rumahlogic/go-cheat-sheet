package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"github.com/mergermarket/go-pkcs7"
	"gitlab.com/rumahlogic/go-cheat-sheet/utils"
	"io"
)

// Cipher key must be 32 chars long because block size is 16 bytes
const CIPHER_KEY = "ahbd7asdhGDnsDJsdbSOpwr3293nsfjnNDSbhgsf8723bhsdbfvg"
var strData = "1a572b1dcee4ffb18440683bfe99a9dbc0a10e4efc4633e4d5984ae52065d8ba"


func main() {
	//utils.LogPrint(Decrypt(strData))
	utils.LogPrint("4f3ce6ddcc29bb42176a2bc6fdbec3101b3c5c226d4028cf7ffde2dd62849674")
	utils.LogPrint(sha256.Sum256([]byte("myHashedIV")))
	utils.LogPrint(sha256.New().Write([]byte("myHashedIV")))
}


// Encrypt encrypts plain text string into cipher text string
func Encrypt(unencrypted string) (string, error) {
	key := []byte(CIPHER_KEY)
	plainText := []byte(unencrypted)
	plainText, err := pkcs7.Pad(plainText, aes.BlockSize)
	if err != nil {
		return "", fmt.Errorf(`plainText: "%s" has error`, plainText)
	}
	if len(plainText)%aes.BlockSize != 0 {
		err := fmt.Errorf(`plainText: "%s" has the wrong block size`, plainText)
		return "", err
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	cipherText := make([]byte, aes.BlockSize+len(plainText))
	iv := cipherText[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}

	mode := cipher.NewCBCEncrypter(block, iv)
	mode.CryptBlocks(cipherText[aes.BlockSize:], plainText)

	return fmt.Sprintf("%x", cipherText), nil
}

// Decrypt decrypts cipher text string into plain text string
func Decrypt(encrypted string) (string, error) {
	key := []byte(CIPHER_KEY)
	cipherText, _ := hex.DecodeString(encrypted)

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	if len(cipherText) < aes.BlockSize {
		panic("cipherText too short")
	}
	iv := cipherText[:aes.BlockSize]
	cipherText = cipherText[aes.BlockSize:]
	if len(cipherText)%aes.BlockSize != 0 {
		panic("cipherText is not a multiple of the block size")
	}

	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(cipherText, cipherText)

	cipherText, _ = pkcs7.Unpad(cipherText, aes.BlockSize)
	return fmt.Sprintf("%s", cipherText), nil
}

