package main

import (
	"gitlab.com/rumahlogic/go-cheat-sheet/utils"
	"strings"
)

func main() {
	body, err := utils.NewRequest("POST", "http://192.168.51.4:8082/auth/user/detail/employee", strings.NewReader(`{"uuid_user": "23f97248-d484-4747-a73b-36aec78ba4dd"}`), "Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVVUlEIjoiMDdmMDBhMjctNGM5ZS00MGIyLWI5OGQtNWI2YzFhNzEzOTk5In0.699Eq0nQEKV1Rs_2cjovVg0hcWX2J_zdq0sT5Yfi92s")

	utils.ErrorHandler(err)
	utils.LogPrint(string(body))
}
