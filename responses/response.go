package responses

import (
	"errors"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
	"gorm.io/gorm"
	"net/http"
)

type Data struct {
	Result     interface{} `json:"result"`
	Pagination interface{} `json:"pagination"`
}

type Response struct {
	Code    string      `json:"code,omitempty"`
	Data    interface{} `json:"data,omitempty"`
	Extra   interface{} `json:"extra,omitempty"`
	IsError bool        `json:"is_error"`
	Message string      `json:"message"`
	Status  int         `json:"status"`
}

//M for message
type M struct {
	//C for code
	C string `json:"c"`
	//M for message
	M string `json:"m"`
}

func loopArgs(ctx *gin.Context, statusCode int, msg M, args []interface{}) (res Response) {
	//Init value
	res.Code = msg.C
	res.IsError = false
	res.Message = msg.M
	res.Status = statusCode

	//if error
	if statusCode > 201 {
		ctx.Writer.Header().Set("X-ERROR-CODE", msg.C)
		ctx.Writer.Header().Set("X-ERROR-MESSAGE", msg.M)
		res.IsError = true
		return
	}

	//if success
	ctx.Writer.Header().Set("X-SUCCESS-CODE", msg.C)
	ctx.Writer.Header().Set("X-SUCCESS-MESSAGE", msg.M)
	for idx, elm := range args {
		if idx == 0 {
			res.Data = elm
		}
		if idx > 0 {
			if len(args[1:]) > 1 {
				res.Extra = args[1:]
			} else {
				res.Extra = elm
			}
		}
	}
	return
}

//func NotFoundOrBadRequestMongo(err error, ctx *gin.Context, msg M, args ...interface{}) {
//	if errors.Is(err, mongo.ErrNoDocuments) {
//		NotFound(ctx, msg, args...)
//		return
//	}
//	BadRequest(ctx, msg, args...)
//	return
//}
//
//func NotFoundOrBadRequestGorm(err error, ctx *gin.Context, msg M, args ...interface{}) {
//	if errors.Is(err, gorm.ErrRecordNotFound) {
//		NotFound(ctx, msg, args...)
//		return
//	}
//	BadRequest(ctx, msg, args...)
//	return
//}
//
//// BadRequest have 3 args (message,data,extra)
//func BadRequest(ctx *gin.Context, msg M, args ...interface{}) {
//	ctx.SecureJSON(http.StatusBadRequest, loopArgs(ctx, http.StatusBadRequest, msg, args))
//	return
//}
//
//// Ok have 3 args (message,data,extra)
//func Ok(ctx *gin.Context, msg M, args ...interface{}) {
//	ctx.SecureJSON(http.StatusOK, loopArgs(ctx, http.StatusOK, msg, args))
//	return
//}
//
//// Created have 3 args (message,data,extra)
//func Created(ctx *gin.Context, msg M, args ...interface{}) {
//	ctx.SecureJSON(http.StatusCreated, loopArgs(ctx, http.StatusCreated, msg, args))
//	return
//}
//
//// NotAcceptable have 3 args (message,data,extra)
//func NotAcceptable(ctx *gin.Context, msg M, args ...interface{}) {
//	ctx.SecureJSON(http.StatusNotAcceptable, loopArgs(ctx, http.StatusNotAcceptable, msg, args))
//	return
//}
//
//// Unauthorized have 3 args (message,data,extra)
//func Unauthorized(ctx *gin.Context, msg M, args ...interface{}) {
//	ctx.SecureJSON(http.StatusUnauthorized, loopArgs(ctx, http.StatusUnauthorized, msg, args))
//	return
//}
//
//// NotFound have 3 args (message,data,extra)
//func NotFound(ctx *gin.Context, msg M, args ...interface{}) {
//	ctx.SecureJSON(http.StatusNotFound, loopArgs(ctx, http.StatusNotFound, msg, args))
//	return
//}
//
//// Gone have 3 args (message,data,extra)
//func Gone(ctx *gin.Context, msg M, args ...interface{}) {
//	ctx.SecureJSON(http.StatusGone, loopArgs(ctx, http.StatusGone, msg, args))
//	return
//}
//
//// Forbidden have 3 args (message,data,extra)
//func Forbidden(ctx *gin.Context, msg M, args ...interface{}) {
//	ctx.SecureJSON(http.StatusForbidden, loopArgs(ctx, http.StatusForbidden, msg, args))
//	return
//}
//
//// MethodNotAllowed have 3 args (message,data,extra)
//func MethodNotAllowed(ctx *gin.Context, msg M, args ...interface{}) {
//	ctx.SecureJSON(http.StatusMethodNotAllowed, loopArgs(ctx, http.StatusMethodNotAllowed, msg, args))
//	return
//}

func NotFoundOrBadRequestMongo(err error, ctx *gin.Context, msg M, args ...interface{}) {
	if errors.Is(err, mongo.ErrNoDocuments) {
		NotFound(ctx, msg, args...)
		return
	}
	BadRequest(ctx, msg, args...)
	return
}

func NotFoundOrBadRequestGorm(err error, ctx *gin.Context, msg M, args ...interface{}) {
	if errors.Is(err, gorm.ErrRecordNotFound) {
		NotFound(ctx, msg, args...)
		return
	}
	BadRequest(ctx, msg, args...)
	return
}

// Ok have 3 args (message,data,extra)
func Ok(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(http.StatusOK, loopArgs(ctx, http.StatusOK, msg, args))
	return
}

// Created have 3 args (message,data,extra)
func Created(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(http.StatusOK, loopArgs(ctx, http.StatusCreated, msg, args))
	return
}

// NoContent have 3 args (message,data,extra)
func NoContent(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.Status(http.StatusNoContent)
	return
}

// Client error responses (4xx)

// BadRequest have 3 args (message,data,extra)
func BadRequest(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(http.StatusOK, loopArgs(ctx, http.StatusBadRequest, msg, args))
	return
}

// Unauthorized have 3 args (message,data,extra)
func Unauthorized(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(http.StatusOK, loopArgs(ctx, http.StatusUnauthorized, msg, args))
	return
}

// PaymentRequired have 3 args (message,data,extra)
func PaymentRequired(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(http.StatusOK, loopArgs(ctx, http.StatusPaymentRequired, msg, args))
	return
}

// Forbidden have 3 args (message,data,extra)
func Forbidden(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(http.StatusOK, loopArgs(ctx, http.StatusForbidden, msg, args))
	return
}

// NotFound have 3 args (message,data,extra)
func NotFound(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(http.StatusOK, loopArgs(ctx, http.StatusNotFound, msg, args))
	return
}

// MethodNotAllowed have 3 args (message,data,extra)
func MethodNotAllowed(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(http.StatusOK, loopArgs(ctx, http.StatusMethodNotAllowed, msg, args))
	return
}

// NotAcceptable have 3 args (message,data,extra)
func NotAcceptable(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(http.StatusOK, loopArgs(ctx, http.StatusNotAcceptable, msg, args))
	return
}

// ProxyAuthRequired have 3 args (message,data,extra)
func ProxyAuthRequired(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(http.StatusOK, loopArgs(ctx, http.StatusProxyAuthRequired, msg, args))
	return
}

// RequestTimeOut have 3 args (message,data,extra)
func RequestTimeOut(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(http.StatusOK, loopArgs(ctx, http.StatusRequestTimeout, msg, args))
	return
}

// Conflict have 3 args (message,data,extra)
func Conflict(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(http.StatusOK, loopArgs(ctx, http.StatusConflict, msg, args))
	return
}

// Gone have 3 args (message,data,extra)
func Gone(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(http.StatusOK, loopArgs(ctx, http.StatusGone, msg, args))
	return
}

// ToManyRequests have 3 args (message,data,extra)
func ToManyRequests(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(http.StatusOK, loopArgs(ctx, http.StatusTooManyRequests, msg, args))
	return
}
