package utils

import (
	"strconv"
	"strings"
)

// EqualString for cek if string contain substring
// example : EqualString("Report Service", "report") will return true.
func EqualString(str, subStr string) bool {
	if ok := strings.Index(strings.ToLower(str), strings.ToLower(subStr)); ok > -1 {
		return true
	}

	return false
}

//StringToUintArr for convert array of string number to unit array
func StringToUintArr(req []string) (res []uint) {
	for _, val := range req {
		num, _ := strconv.Atoi(val)

		res = append(res, uint(num))
	}
	return
}

// IsContainString Check if array string contain string
func IsContainString(str string, data []string) bool {
	for _, val := range data {
		if val == str { return true }
	}
	return false
}
