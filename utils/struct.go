package utils

import "encoding/json"

// Convert for convert from map[string]interface{} to struct or reverse or array of them
func Convert(source interface{}, destination interface{}) {
	var (
		jsonBody	[]byte
		err 		error
	)

	if val, ok := source.(string); ok {
		jsonBody = []byte(val)
	} else if val1, ok1 := source.([]byte); ok1 {
		jsonBody = val1
	} else {
		if jsonBody, err = json.Marshal(source); err != nil {
			ErrorHandler(err)
		}
	}

	ErrorHandler(json.Unmarshal(jsonBody, destination))
	return
}
