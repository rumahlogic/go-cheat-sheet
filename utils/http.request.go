package utils

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"strings"
)


// NewRequest is func to make http request using param method(string), url(string) and payload(*strings.Reader)
// you can add custom header with param after payload as long as it consists of repeated keys, values and cannot be odd
// Example :
// if body, err := NewRequest("GET", "https://ipinfo.io/json", nil, nil); err != nill {
//	ErrorHandler(err)
//	return err
//}
func NewRequest(method, url string, payload interface{}, headers ...string) (body []byte, err error) {

	var (
		client	= &http.Client{}
		req 	*http.Request
		res 	*http.Response
	)

	if payload != nil {
		if val, ok := payload.(*strings.Reader); ok {
			req, err = http.NewRequest(method, url, val)
		}
		if val, ok := payload.(*bytes.Buffer); ok {
			req, err = http.NewRequest(method, url, val)
		}
	} else {
		req, err = http.NewRequest(method, url, nil)
	}

	if  err != nil {
		ErrorHandler(err)
		return
	} 

	if len(headers) > 1 {
		for id := range headers {
			if id % 2 == 1 {
				req.Header[headers[id-1]] = []string{headers[id]}
			}
		}
	}
	if payload != nil { req.Header.Add("Content-Type", "application/json") }

	if res, err = client.Do(req); err != nil {
		ErrorHandler(err)
		return
	}
	defer CloseBody(res.Body)

	if body, err = ioutil.ReadAll(res.Body); err != nil {
		ErrorHandler(err)
		return
	}

	return
}
