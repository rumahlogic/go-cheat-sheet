package utils

import (
	"encoding/json"
	"fmt"
	"os"
	"runtime"
	"time"
)

// PrettyPrint for print struct/map/array to console as indent view
func PrettyPrint(data interface{}) {
	if data != nil {
		p, err := json.MarshalIndent(data, "", "\t")
		if err != nil {
			ErrorHandler(err)
			return
		}
		fmt.Printf("[%s]\n%s \n", GetEnvString("ENGINE_NAME", "UNDEFINED"), p)
	}

}

// ErrorHandler for print error message to console with engine name if error != nil
func ErrorHandler(err error) {
	if err != nil {
		var (
			_, file, line, _ = runtime.Caller(1)
			loc, _           = time.LoadLocation("Asia/Jakarta")
			timeStr          = time.Now().In(loc).Format("2006/01/02 15:04:05")
		)

		fmt.Printf("[%s] %s\n%s %s:%d\n", GetEnvString("ENGINE_NAME", "UNDEFINED"), err.Error(), timeStr, file, line)
	}
}

// LogPrint for print message for debug with engine name
func LogPrint(args ...interface{}) {
	if true {
		fmt.Printf("[%s] ", GetEnvString("ENGINE_NAME", "UNDEFINED"))
		_, _ = fmt.Fprintln(os.Stdout, args...)
	}
}
