package databases

import (
	"context"
	"fmt"
	"gitlab.com/rumahlogic/go-cheat-sheet/utils"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

type Mongo struct {
	Config 	Config
	Db *mongo.Database
	Client *mongo.Client
}

func (mdb *Mongo) Connect() (err error) {
	var (
		cancel 	context.CancelFunc
		ctx 	context.Context
	)

	if mdb.Client, err = mongo.NewClient(options.Client().
		ApplyURI(fmt.Sprintf("mongodb://%s:%s", mdb.Config.HOST,mdb.Config.PORT)).
		SetAuth(options.Credential{
			AuthSource: mdb.Config.NAME,
			Username: mdb.Config.USER,
			Password: mdb.Config.PASS,
		})); err != nil {
		utils.ErrorHandler(err)
		panic(err)
	}

	ctx, cancel = context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err = mdb.Client.Connect(ctx); err != nil {
		utils.ErrorHandler(err)
		panic(err)
	}
	mdb.Db = mdb.Client.Database(mdb.Config.NAME)

	return
}

func (mdb *Mongo) Collection(col string) (collection *mongo.Collection) {
	return mdb.Db.Collection(col)
}