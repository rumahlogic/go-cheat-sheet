package databases

import (
	"database/sql"
	"gitlab.com/rumahlogic/go-cheat-sheet/utils"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type SQLite struct {
	Config Config
}

func (sl *SQLite) Connect(cfg *gorm.Config) (DB *gorm.DB, sql_ *sql.DB) {

	var err	error

	if cfg == nil {
		cfg = &gorm.Config{}
	}

	if DB, err = gorm.Open(sqlite.Open(sl.Config.NAME), cfg); err != nil {
		utils.ErrorHandler(err)
		panic(err)
	}

	if sql_, err = DB.DB(); err != nil {
		utils.ErrorHandler(err)
		panic(err)
	}
	return
}